const imgContainer = document.querySelectorAll(".images-wrapper img");
const timerElement = document.querySelector(".timer");
const sliderStopper = document.querySelector(".slider-stopper");
const sliderPlayer = document.querySelector(".slider-player");
let currentIndex = 0;
let intervalId;

sliderStopper.addEventListener("click", stopSlider);
sliderPlayer.addEventListener("click", startSlider);

function startSlider() {
        clearInterval(intervalId);
        sliderPlayer.disabled = true; 
    timerElement.style.color = "blue";
    showPicture(currentIndex);
    updateTimer(3000);

    intervalId = setInterval(() => {
        hidePicture(currentIndex);
        currentIndex += 1;
        if (currentIndex === imgContainer.length) {
            currentIndex = 0;
        }
        showPicture(currentIndex);
        updateTimer(3000);
        sliderPlayer.style.display = "block";
        sliderStopper.style.display = "block";
    }, 3000);
}

function showPicture(index) {
    imgContainer[index].style.display = "block";
}

function hidePicture(index) {
    imgContainer[index].style.display = "none";
}

function stopSlider() {
    clearInterval(intervalId);
    timerElement.style.color = "red";
    sliderPlayer.disabled = false;
}

function updateTimer(duration) {
    let millisecondsLeft = duration;

    const timerId = setInterval(() => {
        millisecondsLeft -= 10; // decrease by 10 milliseconds

        // Calculate remaining time in seconds and milliseconds
        const seconds = Math.floor(millisecondsLeft / 1000);
        const milliseconds = millisecondsLeft - (seconds * 1000);

        timerElement.textContent = `${seconds}.${milliseconds.toString().padStart(3, '0')}`;

        if (millisecondsLeft <= 0) {
            clearInterval(timerId);
        }
    }, 10);
}

window.addEventListener("DOMContentLoaded", startSlider);
window.addEventListener("unload", stopSlider);

